package com.oreillyauto.problems.carInheritance;

public class HondaCity extends Car {
    int mileage = 0;
    
    
    public HondaCity(int mileage) {
        super(true, "4");
        this.mileage = mileage;
        // TODO Auto-generated constructor stub
    }

    @Override
    public String getMileage() {
        // TODO Auto-generated method stub
        return mileage + " kmpl";
    }

}
