package com.oreillyauto.problems.compusa;

public class Laptop {
    String SKU;
    String serialNumber;
    int laptopCount;
    
    Laptop(String SKU, String serialNumber) {
        this.SKU = SKU;
        this.serialNumber = serialNumber;
    }
    
    String getSerialNumber() {
        return serialNumber;
    }
    
    String getSKU() {
        return SKU;
    }
    
    int getLaptopCount() {
        return laptopCount;
    }
    
    void setLaptopCount(int laptopCount) {
        this.laptopCount = laptopCount;
    }
}
