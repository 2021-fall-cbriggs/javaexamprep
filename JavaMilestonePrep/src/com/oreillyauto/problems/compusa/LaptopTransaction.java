package com.oreillyauto.problems.compusa;

public class LaptopTransaction {
    static String addLaptop(Laptop laptop, int count) throws TransactionException {
        if (laptop.getSerialNumber() == null) {
            throw new TransactionException("Serial number Missing.", "SERIAL_NUMBER_MISSING");
        }
        if (count < 1) {
            throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
        }
        
        laptop.setLaptopCount(laptop.getLaptopCount() + count);
        return "Laptops successfully added.";
    }

    static String sellLaptop(Laptop laptop, int count) throws TransactionException {
        if (laptop.getSerialNumber() == null || laptop.getSerialNumber() == "" ||laptop.getSerialNumber().trim() == "") {
            throw new TransactionException("Serial number Missing", "SERIAL_NUMBER_MISSING");
        }
        if (laptop.getLaptopCount() < 1) {
            throw new TransactionException("Out of stock.", "OUT_OF_STOCK");
        }
        if (count < 1) {
            throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
        }
        laptop.setLaptopCount(laptop.getLaptopCount() - count);
        return "Laptops successfully sold.";

    }
}
