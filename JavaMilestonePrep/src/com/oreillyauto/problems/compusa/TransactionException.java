package com.oreillyauto.problems.compusa;

public class TransactionException extends Exception {
    String errorMessage;
    String errorCode;
    
    TransactionException(String errorMessage, String errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }
    
    String getErrorCode() {
        return errorCode;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
}
