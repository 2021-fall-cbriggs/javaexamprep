package com.oreillyauto.problems.cuisine;

import java.util.HashMap;
import java.util.Map;

public class FoodFactory {

    static FoodFactory factory; // Singleton instance of this class. Notice it is static!
    Map<String, Cuisine> cuisineMap = new HashMap<String, Cuisine>();
    
    public static FoodFactory getFactory() {
        if (factory == null) {
            factory = new FoodFactory();
        }
        return factory;
    }
    
    public void registerCuisine(String cuisineKey, Cuisine cuisine) {
        cuisineMap.put(cuisineKey, cuisine);
    }

    public Cuisine serveCuisine(String cuisine, String dish) throws UnservableCuisineRequestException {
        if(cuisineMap.containsKey(cuisine)) {
            return cuisineMap.get(cuisine).serveFood(dish);
        }
        throw new UnservableCuisineRequestException("Unservable cuisine " + cuisine + " for dish Marjoram");
    }

}
