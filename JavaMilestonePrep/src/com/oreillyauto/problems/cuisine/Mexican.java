package com.oreillyauto.problems.cuisine;

public class Mexican extends Cuisine {
    String dish;

    @Override
    public Cuisine serveFood(String dish) {
        this.dish = dish;
        return this;
    }

    public String getDish() {
        // TODO Auto-generated method stub
        return dish;
    }

}