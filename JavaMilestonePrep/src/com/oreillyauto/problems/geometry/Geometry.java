package com.oreillyauto.problems.geometry;

public class Geometry implements Square, Triangle{
    public int area(int length) {
        return length * length;
    }
    
    public int area(int base, int height) {
        return base * height / 2;
    }
}
