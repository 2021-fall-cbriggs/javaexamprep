package com.oreillyauto.problems.robot;

public class Robot {
    Integer currentX;
    Integer currentY;
    Integer previousX;
    Integer previousY;
    Integer lastChange;
    Boolean xMovedLast = false;

    Robot() {
        currentX = 0;
        currentY = 5;
        previousX = 0;
        previousY = 5;
    }

    Robot(Integer x, Integer y) {
        currentX = x;
        currentY = y;
        previousX = 0;
        previousY = 5;
    }

    void moveX(Integer dx) {
        previousX = currentX + dx;
        currentX += dx;
        lastChange = dx;
        xMovedLast = true;
    }

    void moveY(Integer dy) {
        previousY = currentY;
        currentY += dy;
        lastChange = dy;
        xMovedLast = false;
    }

    String printCurrentCoordinates() {
        return Integer.toString(currentX) + " " + Integer.toString(currentY);
    }

    String printLastCoordinates() {
        return Integer.toString(previousX) + " " + Integer.toString(previousY);
    }

    String printLastMove() {
        if (xMovedLast) {
            return "x " + lastChange;
        } else {
            return "y " + lastChange;
        }
    }
}
