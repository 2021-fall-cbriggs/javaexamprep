package com.oreillyauto.problems.volume;

abstract class Volume {
    protected int gallons;
    protected float ounces;
    
    abstract void setGallonsAndOunces(int gallons, float ounces);
    abstract int getGallons();
    abstract float getOunces();
    abstract String getVolumeComparison(Volume volume);
}