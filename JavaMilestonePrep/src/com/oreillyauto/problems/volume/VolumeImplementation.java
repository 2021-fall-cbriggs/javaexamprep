package com.oreillyauto.problems.volume;

public class VolumeImplementation extends Volume {
    int gallons;
    float ounces;
    
    @Override
    void setGallonsAndOunces(int gallons, float ounces) {
        // TODO Auto-generated method stub
        this.gallons = gallons;
        this.ounces = ounces;
    }

    @Override
    int getGallons() {
        // TODO Auto-generated method stub
        return gallons;
    }

    @Override
    float getOunces() {
        // TODO Auto-generated method stub
        return ounces;
    }

    @Override
    String getVolumeComparison(Volume volume) {
        // TODO Auto-generated method stub
        String response = "";
        if(this.gallons > volume.getGallons()) {
            response = "First volume is greater.";
        } else if(this.gallons < volume.getGallons()) {
            response = "Second volume is greater.";
        } else if(this.gallons == volume.getGallons()) {
            if(this.ounces > volume.getOunces()) {
                response = "First volume is greater.";
            } else if(this.ounces < volume.getOunces()) {
                response = "Second volume is greater.";
            }
        }
        return response;
    }

}
